module.exports = {
  exts: [{
    name: 'import-postman'
  },{
    name: 'import-har'
  },{
    name: 'advanced-mock'
  },{
    name: 'import-swagger'
  },{
    name: 'statistics'
  },{
    name: 'export-data'
  },{
    name: 'gen-services'
  },{
    name: 'export-swagger2-data'
  },{
    name: 'import-yapi-json'
  },{
    name: 'wiki'
  }, {
    name: 'swagger-auto-sync'
  }, {
      name: 'typeql-class',
      options:{}
  }, {
    name: 'cfsso',
    options: {
      type: "sso",
      domain:'http://login.jiayun.club:8080',
      client:"http://login.jiayun.club:8080/ssoAuthToken/save",
      server:{
        login:"http://login.jiayun.club:8080/login2",
        check:"http://login.jiayun.club:8080/ssoAuthToken/check",
        user:"http://login.jiayun.club:8080/get/user/info/bySsoAuthToken",
        logout:"http://login.jiayun.club:8080/logout"
      },
      loginUrl: "http://sso.example.com/service/verifytoken.php?token=",
      emailPostfix: "@clubfactory.com",
      AUTH_SERVER : "https://sso.example.com/login.php"
    }
  }
]
}