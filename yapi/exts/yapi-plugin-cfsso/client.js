import React from 'react'

const qualifyURL = (url, encode) => {
  url = url || '';
  var ret = location.protocol + '//' + location.host + (url.substr(0, 1) === '/' ? '' : location.pathname.match(/.*\//)) + url;
  if (encode) {
    ret = encodeURIComponent(ret);
  }
  return ret;
}

module.exports = function (options) {
  const { server } = options;
  const handleLogin = () => {
    // const loginURI = '/api/user/login_by_token';
    const loginURI = '/group'
    let ret = qualifyURL(loginURI, true);
    let redirectURL = `${server.login}?redirectUrl=${ret}&loginType=1`;
    location.href = redirectURL;
  }

  const CFssoComponent = () => (
    <button onClick={handleLogin} className="btn-home btn-home-normal" >CF-SSO 登录</button>
  )

  this.bindHook('third_login', CFssoComponent);
};