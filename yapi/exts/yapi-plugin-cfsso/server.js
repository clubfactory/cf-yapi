const request = require('request');

const userController = require('../../server/controllers/user.js');

module.exports = function (options) {
  const { server } = options;

  //绑定cfsso回调 /ssoAuthToken/save?token=xxx
  this.bindHook('add_router', function(addRouter){
    addRouter({
      controller: userController,
      method: 'get',
      routerPath: '/save',
      next:'/ssoAuthToken',
      action: 'loginByToken',
      path:'/'
    })
  })

  // 第三方登录 钩子
  this.bindHook('third_login', (ctx) => {
    let token = ctx.request.body.token || ctx.request.query.token;
    return new Promise((resolve, reject) => {
      request(`${server.user}?token=${token}`, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          let result = JSON.parse(body);
          if (result && result.success === true) {
            const { data } = result
            let ret = {
              email: data.email,
              username: data.name
            };
            resolve(ret);
          } else {
            reject(result);
          }
        }
        reject(error);
      });
    });
  })
}